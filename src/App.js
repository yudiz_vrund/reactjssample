import React from 'react';
import FunctionEx from './FunctionEx';
import ClassEx from './ClassEx';
import StateEx from './StateEx';
import StateExWithClass from './StateExWithClass';
import PropExample from './PropExample';

const App = () => {
    return(
        <>
        <div>
            <h4>Normal Function Example</h4>
            <FunctionEx name={"Vrund"} ></FunctionEx>
            <h4>Normal Class Example</h4>
            <ClassEx/>
            <h4>Normal Function with state and decrement counter Example</h4>
            <StateEx/>
            <h4>Normal Class with state and increment counter Example</h4>
            <StateExWithClass/>
            <h4>Checking Props Example</h4>
            <PropExample />
        </div>
        </>
    )
}

export default App;